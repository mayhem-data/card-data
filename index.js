const csvtojson = require("csvtojson");
const fs = require('fs');

const config = {
  dataFolder: './data/',
  distFolder: './dist/',
  inputFileName: 'cards.tsv',
  outputFileName: 'cards.json'
}

// as per `preFileLine` api, 2 arguments
const headingToCamelCase = (fileLine, lineIndex) => {
  if (lineIndex === 0) { // is this the first line?
    return fileLine.replace(/\ ([a-zA-Z])/g, (_, a) => a.toUpperCase()); // replace
  } else {
    return fileLine; // Don't touch, just return it.
  }
};

csvtojson({checkType: true, delimiter: '\t', ignoreEmpty: true})
  .fromFile(config.dataFolder + config.inputFileName)
  .preFileLine(headingToCamelCase)
  .then(json =>
    fs.writeFileSync(config.distFolder + config.outputFileName, JSON.stringify(json)));
